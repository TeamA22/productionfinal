package dbService;

import entity.UserProfile;
import responce.Deal;
import responce.Instrument;
import responce.InstrumentsResponse;
import responce.Response;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBService {
    private final Connection connection;

    public DBService() {
        this.connection = getMysqlConnection();
    }

    public static void main(String[] args) {
        try {
            new DBService().getDeals();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getMysqlConnection() {
        try {
            DriverManager.registerDriver((Driver) Class.forName("com.mysql.jdbc.Driver").newInstance());

            StringBuilder url = new StringBuilder();

            url.
                    append("jdbc:mysql://").        //db type
                    append("localhost:").           //host name
                    append("3307/").                //port
                    append("db_grad_cs_1917?").          //db name
                    append("user=root&").          //login
                    append("password=ppp");       //password

            System.out.println("URL: " + url + "\n");

            Connection connection = DriverManager.getConnection(url.toString());
            return connection;
        } catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public InstrumentsResponse getInstruments() throws SQLException {
        InstrumentsResponse instrumentsResponse =
                new InstrumentsResponse();

        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;

        List<String> namesOfInstruments = new ArrayList<>();

        String sqlNames = "SELECT distinct i.instrument_name " +
                "FROM db_grad_cs_1917.deal as d, db_grad_cs_1917.instrument as i " +
                "WHERE d.deal_instrument_id=i.instrument_id";

        String sqlInstruments = "SELECT" +
                " d.deal_id," +
                " d.deal_time," +
                " d.deal_type," +
                " d.deal_amount," +
                " d.deal_quantity," +
                " i.instrument_name," +
                " c.counterparty_name," +
                " c.counterparty_status," +
                " c.counterparty_date_registered" +
                " FROM db_grad_cs_1917.deal as d, db_grad_cs_1917.counterparty as c, db_grad_cs_1917.instrument as i" +
                " WHERE d.deal_instrument_id=i.instrument_id and d.deal_counterparty_id=c.counterparty_id" +
                " and i.instrument_name = ? ORDER BY d.deal_time;";

        try {
            dbConnection = connection;
            preparedStatement = dbConnection.prepareStatement(sqlNames);

            // execute select SQL stetement
            ResultSet rs = preparedStatement.executeQuery();
            String instrumentName;
            while (rs.next()) {
                instrumentName = rs.getString("instrument_name");
                namesOfInstruments.add(instrumentName);
            }

            for (String nameOfInstrument : namesOfInstruments) {
                preparedStatement = dbConnection.
                        prepareStatement(sqlInstruments);
                preparedStatement.setString(1, nameOfInstrument);
                ResultSet instrumentsSet = preparedStatement.executeQuery();

                Instrument instrument = new Instrument(nameOfInstrument);
                while (instrumentsSet.next()) {
                    String dealId = instrumentsSet.getString("deal_id");
                    String dealQuantity = instrumentsSet.getString("deal_quantity");
                    String dealTime = instrumentsSet.getString("deal_time");
                    String dealType = instrumentsSet.getString("deal_type");
                    String dealAmount = instrumentsSet.getString("deal_amount");
                    String insName = instrumentsSet.getString("instrument_name");
                    String counterpartyName = instrumentsSet.getString("counterparty_name");
                    String counterpartyStatus = instrumentsSet.getString("counterparty_status");
                    String counterpartyDateRegistered = instrumentsSet.getString("counterparty_date_registered");

                    Deal deal = new Deal();
                    deal.setDealId(dealId);
                    deal.setDealQuantity(dealQuantity);
                    deal.setDealTime(dealTime);
                    deal.setDealType(dealType);
                    deal.setDealAmount(dealAmount);
                    deal.setInstrumentName(insName);
                    deal.setCounterpartyName(counterpartyName);
                    deal.setCounterpartyStatus(counterpartyStatus);
                    deal.setCounterpartyDateRegistered(counterpartyDateRegistered);

                    instrument.addDeal(deal);
                }
                instrumentsResponse.addInstrument(instrument);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }

        return instrumentsResponse;
    }

    public Response getDeals() throws SQLException {
        Response responce = new Response();

        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;

        String selectSQL = "SELECT d.deal_id," +
                " d.deal_time," +
                " d.deal_type," +
                " d.deal_amount," +
                " d.deal_quantity," +
                " i.instrument_name," +
                " c.counterparty_name," +
                " c.counterparty_status," +
                " c.counterparty_date_registered" +
                " FROM db_grad_cs_1917.deal as d, db_grad_cs_1917.counterparty as c, db_grad_cs_1917.instrument as i " +
                " WHERE d.deal_instrument_id=i.instrument_id and d.deal_counterparty_id=c.counterparty_id;";

        try {
            dbConnection = connection;
            preparedStatement = dbConnection.prepareStatement(selectSQL);

            // execute select SQL stetement
            ResultSet rs = preparedStatement.executeQuery();


            while (rs.next()) {

                String dealId = rs.getString("deal_id");
                String dealQuantity = rs.getString("deal_quantity");
                String dealTime = rs.getString("deal_time");
                String dealType = rs.getString("deal_type");
                String dealAmount = rs.getString("deal_amount");
                String instrumentName = rs.getString("instrument_name");
                String counterpartyName = rs.getString("counterparty_name");
                String counterpartyStatus = rs.getString("counterparty_status");
                String counterpartyDateRegistered = rs.getString("counterparty_date_registered");


                Deal deal = new Deal();
                deal.setDealId(dealId);
                deal.setDealQuantity(dealQuantity);
                deal.setDealTime(dealTime);
                deal.setDealType(dealType);
                deal.setDealAmount(dealAmount);
                deal.setInstrumentName(instrumentName);
                deal.setCounterpartyName(counterpartyName);
                deal.setCounterpartyStatus(counterpartyStatus);
                deal.setCounterpartyDateRegistered(counterpartyDateRegistered);

                System.out.println(deal);
                responce.add(deal);
            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (preparedStatement != null) {
                preparedStatement.close();
            }

        }

        return responce;

    }

    public void printConnectInfo() {
        try {
            System.out.println("DB name: " + connection.getMetaData().getDatabaseProductName());
            System.out.println("DB version: " + connection.getMetaData().getDatabaseProductVersion());
            System.out.println("Driver: " + connection.getMetaData().getDriverName());
            System.out.println("Autocommit: " + connection.getAutoCommit());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<UserProfile> getUsersOnStartUp() {

        List<UserProfile> userProfiles = new ArrayList<>(10);
        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;

        String selectSQL = "SELECT user_id, user_pwd " +
                "FROM db_grad_cs_1917.users";

        try {
            dbConnection = connection;
            preparedStatement = dbConnection.prepareStatement(selectSQL);

            // execute select SQL stetement
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {

                String userId = rs.getString("user_id");
                String userPwd = rs.getString("user_pwd");

                System.out.println("-------------------");
                System.out.println(userId);
                System.out.println(userPwd);

                UserProfile userProfile = new UserProfile(userId,
                        userPwd,
                        userId);
                userProfiles.add(userProfile);
            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
            } catch (SQLException e) {
                System.out.println("Got exception here!");
            }
            return userProfiles;
        }
    }
}
