package listener;

import dbService.DBService;
import entity.UserProfile;
import service.UserProfileService;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.List;

@WebListener
public class StartUpListener implements ServletContextListener {
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        System.out.println("Context is initialised!");
        ServletContext servletContext = servletContextEvent.getServletContext();

        UserProfileService userProfileService = new UserProfileService();
        DBService dbService = new DBService();
        servletContext.setAttribute("user_service", userProfileService);
        servletContext.setAttribute("db", dbService);
        List<UserProfile> userProfileList =
                dbService.getUsersOnStartUp();

        for (UserProfile userProfile : userProfileList) {
            userProfileService.addNewUserToLoginMap(userProfile);
        }
    }

    public void contextDestroyed(ServletContextEvent servletContextEvent) {
//        ServletContext ctx = servletContextEvent.getServletContext();
//        DBConnectionManager dbManager = (DBConnectionManager) ctx.getAttribute("DBManager");
//        dbManager.closeConnection();
//        System.out.println("Database connection closed for Application.");

    }
}



