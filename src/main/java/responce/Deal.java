package responce;

public class Deal {
    private String dealTime;
    private String dealType;
    private String dealAmount;
    private String dealQuantity;
    private String instrumentName;
    private String counterpartyName;
    private String counterpartyStatus;


    private String counterpartyDateRegistered;
    private String dealId;

    @Override
    public String toString() {
        return "Deal{" +
                "dealTime='" + dealTime + '\'' +
                ", dealType='" + dealType + '\'' +
                ", dealAmount='" + dealAmount + '\'' +
                ", dealQuantity='" + dealQuantity + '\'' +
                ", instrumentName='" + instrumentName + '\'' +
                ", counterpartyName='" + counterpartyName + '\'' +
                ", counterpartyStatus='" + counterpartyStatus + '\'' +
                ", dealId='" + dealId + '\'' +
                '}';
    }

    public String getDealTime() {
        return dealTime;
    }

    public void setDealTime(String dealTime) {
        this.dealTime = dealTime;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public String getDealAmount() {
        return dealAmount;
    }

    public void setDealAmount(String dealAmount) {
        this.dealAmount = dealAmount;
    }

    public String getDealQuantity() {
        return dealQuantity;
    }

    public void setDealQuantity(String dealQuantity) {
        this.dealQuantity = dealQuantity;
    }

    public String getInstrumentName() {
        return instrumentName;
    }

    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    public String getCounterpartyName() {
        return counterpartyName;
    }

    public void setCounterpartyName(String counterpartyName) {
        this.counterpartyName = counterpartyName;
    }

    public String getCounterpartyStatus() {
        return counterpartyStatus;
    }

    public void setCounterpartyStatus(String counterpartyStatus) {
        this.counterpartyStatus = counterpartyStatus;
    }

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }


    public String getCounterpartyDateRegistered() {
        return counterpartyDateRegistered;
    }

    public void setCounterpartyDateRegistered(String counterpartyDateRegistered) {
        this.counterpartyDateRegistered = counterpartyDateRegistered;
    }
}


//d.deal_id," +
//        " d.deal_time," +
//        " d.deal_type," +
//        " d.deal_amount," +
//        " d.deal_quantity," +
//        " i.instrument_name," +
//        " c.counterparty_name," +
//        " counterparty_status" +