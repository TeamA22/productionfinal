package responce;

import java.util.ArrayList;
import java.util.List;

public class Instrument {
    private String instrumentName;
    private List<Deal> dealList;

    public Instrument(String instrumentName) {
        this.instrumentName = instrumentName;
        this.dealList = new ArrayList<>();
    }

    public void addDeal(Deal deal) {
        dealList.add(deal);
    }


    public String getInstrumentName() {
        return instrumentName;
    }

    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    public List<Deal> getDealList() {
        return dealList;
    }

    public void setDealList(List<Deal> dealList) {
        this.dealList = dealList;
    }
}
