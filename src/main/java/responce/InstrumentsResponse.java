package responce;

import java.util.ArrayList;
import java.util.List;

public class InstrumentsResponse {
    private List<Instrument> instrumentList;

    public InstrumentsResponse() {
        instrumentList = new ArrayList<>();
    }

    public void addInstrument(Instrument instrument) {
        instrumentList.add(instrument);
    }

    public List<Instrument> getInstrumentList() {
        return instrumentList;
    }

    public void setInstrumentList(List<Instrument> instrumentList) {
        this.instrumentList = instrumentList;
    }
}
