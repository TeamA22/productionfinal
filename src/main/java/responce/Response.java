package responce;

import java.util.ArrayList;
import java.util.List;

public class Response {
    static private int INITIAL_CAPACITY = 2000;
    List<Deal> deals;

    public Response() {
        deals = new ArrayList<Deal>(INITIAL_CAPACITY);
    }

    public void add(Deal deal) {
        deals.add(deal);
    }

    public List<Deal> getDeals() {
        return deals;
    }
}
