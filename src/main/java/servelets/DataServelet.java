package servelets;

import com.fasterxml.jackson.databind.ObjectMapper;
import dbService.DBService;
import responce.Response;
import service.UserProfileService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet(name = "dataservelet", urlPatterns = {"/data"}, loadOnStartup = 1)
public class DataServelet extends HttpServlet {

    static final int UNAUTHORIZED = 401;

    private UserProfileService userProfileService;
    private DBService dbService;

    @Override
    public void init() throws ServletException {
        userProfileService = (UserProfileService) getServletContext().getAttribute("user_service");
        dbService = (DBService) getServletContext().getAttribute("db");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
//        if (userProfileService.getUserBySessionId(session.getId()) == null) {
//            response.setStatus(UNAUTHORIZED);
//            return;
//        }
        ObjectMapper objectMapper = new ObjectMapper();
        response.setContentType("application/json");
        Response responseObject = new Response();
        try {
            responseObject = dbService.getDeals();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        PrintWriter out = response.getWriter();
        String json = objectMapper.writeValueAsString(responseObject);
        out.print(json);
        out.flush();
    }
}
