package servelets;

import entity.UserProfile;
import service.UserProfileService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "LoginServilet", urlPatterns = {"/login"}, loadOnStartup = 1)
public class LoginServelet extends HttpServlet {

    UserProfileService userProfileService;

    @Override
    public void init() throws ServletException {
        userProfileService = (UserProfileService) getServletContext().getAttribute("user_service");
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        HttpSession sessionId = request.getSession();

        UserProfile userProfile = userProfileService.getUserByLogin(email);

        if (userProfile != null && userProfile.getPass().equals(password)) {
            request.getServletContext()
                    .setAttribute("data",
                            userProfileService.log());
            userProfileService.addSession(sessionId.getId(), userProfile);
            request.getRequestDispatcher("WEB-INF/views/dashboard.jsp").
                    forward(request, response);
        } else {
            request.getServletContext()
                    .setAttribute("errMessage",
                            "Wrong login or password!");
            request.getRequestDispatcher("WEB-INF/views/index.jsp").
                    forward(request, response);
        }
        // to-do
        System.out.println(email + " " + password);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserProfile userProfile = userProfileService.getUserBySessionId(request.getSession().getId());

        if (userProfile != null) {
            request.getServletContext()
                    .setAttribute("data",
                            userProfileService.log());
            request.getRequestDispatcher("WEB-INF/views/dashboard.jsp").
                    forward(request, response);
        } else {
            response.setStatus(401);
        }
        // to-do
    }


}
