package servelets;


import entity.UserProfile;
import service.UserProfileService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "RegisterServilet", urlPatterns = {"/register"}, loadOnStartup = 1)
public class RegisterServelet extends HttpServlet {

    UserProfileService userProfileService;

    @Override
    public void init() throws ServletException {
        userProfileService = (UserProfileService) getServletContext().getAttribute("user_service");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("state", "");
        request.getRequestDispatcher("WEB-INF/views/register.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("errMessage", "");

        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String passwordConf = request.getParameter("password-conf");

        if (!passwordConf.equals(password)) {
            request.setAttribute("state", "passwords must be the same!");
            request.getRequestDispatcher("WEB-INF/views/register.jsp").forward(request, response);
        } else if (userProfileService.getUserByLogin(email) == null) {
            UserProfile userProfile = new UserProfile(email, password, email);
            userProfileService.addNewUserToLoginMap(userProfile);
            request.setAttribute("email", email);
            request.getRequestDispatcher("WEB-INF/views/index.jsp").forward(request, response);
        } else {
            request.setAttribute("state", "choose another name!");
            request.getRequestDispatcher("WEB-INF/views/register.jsp").forward(request, response);
        }

        // to-do
        System.out.println(email + " " + password);
    }


}
