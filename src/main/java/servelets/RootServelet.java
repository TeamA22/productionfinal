package servelets;

import service.UserProfileService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "rootservelet", urlPatterns = {"/start"}, loadOnStartup = 1)
public class RootServelet extends HttpServlet {

    UserProfileService userProfileService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("WEB-INF/views/index.jsp").forward(request, response);
    }


}