package service;


import entity.UserProfile;

import java.util.HashMap;
import java.util.Map;

public class UserProfileService {
    private final Map<String, UserProfile> loginToProfile;
    private final Map<String, UserProfile> sessionIdToProfile;

    public UserProfileService() {
        loginToProfile = new HashMap<>();
        sessionIdToProfile = new HashMap<>();
    }

    public void addNewUserToLoginMap(UserProfile userProfile) {
        loginToProfile.put(userProfile.getLogin(), userProfile);
    }

    public UserProfile getUserByLogin(String login) {
        return loginToProfile.get(login);
    }

    public UserProfile getUserBySessionId(String sessionId) {
        return sessionIdToProfile.get(sessionId);
    }

    public void addSession(String sessionId, UserProfile userProfile) {
        sessionIdToProfile.put(sessionId, userProfile);
    }

    public void deleteSession(String sessionId) {
        sessionIdToProfile.remove(sessionId);
    }

    public String log() {
        StringBuilder s = new StringBuilder();
        for (Map.Entry<String, UserProfile> entry : loginToProfile.entrySet()) {
            s.append("<br>" + entry.getKey() + " : " + entry.getValue().getPass());
        }
        return s.toString();
    }
}