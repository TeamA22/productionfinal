
<html>
<head>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/index.css">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type='text/javascript' src='js/jquery.ba-hashchange.min.js'></script>
    <script type='text/javascript' src='js/dynamicpage.js'></script>
    <script src="jquery-3.3.1.min.js"></script>

    <!---    Highcharts  --->
    <script src="js/highcharts.js"></script>
    <script src="js/series-label.js"></script>
    <script src="js/exporting.js"></script>
    <script src="js/export-data.js"></script>
    <!------ Include the above in your HEAD tag ---------->
    <style>
        /*#All {background-color: #ff6961 ;} */
        /*#ShowAll {background-color: #77dd77 ;} */
        #Disclaimer {
            font-size: 12px;
        }

        body {
            background-image: url("http://www.hudsoneq.com/investment-background.png");
            height: 100%;
            background-size: cover;
            background-position: center;
        }

        .main-div {
            background: white;
            border-radius: 2px;
            margin: 10px auto 30px;
            max-width: 100%;
            font-family: "Arial";
            padding: 40px 40px 40px 40px
        }

    </style>

    <script>

        var tablehidden = true
                
                $(document).ready(function () {

                    $("#table_buttons").hide();
                    $("#myTable").hide();
                    $("#table_options").hide();

                    $("#Toggle").click(function () {

                        if (tablehidden == true) {
                            $("#table_buttons").show();
                            $("#myTable").show();
                            $("#table_options").show();
                            $("#container").hide();
                            tablehidden = false;
                        }
                        else {
                            $("#table_buttons").hide();
                            $("#myTable").hide();
                            $("#table_options").hide();
                            $("#container").show();
                            tablehidden = true;
                        }
                    });

                    var Astronomica_off = "tr.Astronomica"
                    var Borealis_off = "tr.Borealis"
                    var Celestial_off = "tr.Celestial"
                    var Deuteronic_off = "tr.Deuteronic"
                    var Eclipse_off = "tr.Eclipse"
                    var Floral_off = "tr.Floral"
                    var Galactia_off = "tr.Galactia"
                    var Heliosphere_off = "tr.Heliosphere"
                    var Interstella_off = "tr.Interstella"
                    var Jupiter_off = "tr.Jupiter"
                    var Koronis_off = "tr.Koronis"
                    var Lunatica_off = "tr.Lunatic"
                    $(Astronomica_off).hide();
                    $(Borealis_off).hide();
                    $(Celestial_off).hide();
                    $(Deuteronic_off).hide();
                    $(Eclipse_off).hide();
                    $(Floral_off).hide();
                    $(Galactia_off).hide();
                    $(Heliosphere_off).hide();
                    $(Interstella_off).hide();
                    $(Jupiter_off).hide();
                    $(Koronis_off).hide();
                    $(Lunatica_off).hide();

                    $("button").click(function () {

                        if (this.id == 'All') {
                            $(Astronomica_off).hide();
                            $(Borealis_off).hide();
                            $(Celestial_off).hide();
                            $(Deuteronic_off).hide();
                            $(Eclipse_off).hide();
                            $(Floral_off).hide();
                            $(Galactia_off).hide();
                            $(Heliosphere_off).hide();
                            $(Interstella_off).hide();
                            $(Jupiter_off).hide();
                            $(Koronis_off).hide();
                            $(Lunatica_off).hide();
                        }

                        if (this.id == 'ShowAll') {
                            $(Astronomica_off).show();
                            $(Borealis_off).show();
                            $(Celestial_off).show();
                            $(Deuteronic_off).show();
                            $(Eclipse_off).show();
                            $(Floral_off).show();
                            $(Galactia_off).show();
                            $(Heliosphere_off).show();
                            $(Interstella_off).show();
                            $(Jupiter_off).show();
                            $(Koronis_off).show();
                            $(Lunatica_off).show();
                        }

                        var instrument_toggle = "tr." + String(this.id)
                        $(instrument_toggle).toggle();
                    });

                    // TABLE VARIABLES THAT WILL BE CHANGED TO BE THE VALUES IN THE TABLE
                    var total_avg_buy = '4,603.68'
                    var total_avg_sell = '4,236.79'
                    var total_eff_pl = '-3,161,295'
                    var total_real_pl = '13,686,894'
                    var total_yield = '-12.14%'
                    var total_volume = '277142'

                    var Astronomica_avg_buy = '2,974.89'
                    var Astronomica_avg_sell = '2,944.10'
                    var Astronomica_eff_pl = '3,351,991'
                    var Astronomica_real_pl = '1,571,794'
                    var Astronomica_yield = '-21.97%'
                    var Astronomica_volume = '26702'

                    var Borealis_avg_buy = '6,110.44'
                    var Borealis_avg_sell = '5,997.63'
                    var Borealis_eff_pl = '-24,965,260'
                    var Borealis_real_pl = '-13,191,685'
                    var Borealis_yield = '4.65%'
                    var Borealis_volume = '26403'

                    var Celestial_avg_buy = '1,223.52'
                    var Celestial_avg_sell = '1,242.56'
                    var Celestial_eff_pl = '4,219,152'
                    var Celestial_real_pl = '2,308,916'
                    var Celestial_yield = '-22.92%'
                    var Celestial_volume = '17432'

                    var Deuteronic_avg_buy = '5,495.59'
                    var Deuteronic_avg_sell = '5,278.17'
                    var Deuteronic_eff_pl = '34,513,967'
                    var Deuteronic_real_pl = '20,532,110'
                    var Deuteronic_yield = '-62.61%'
                    var Deuteronic_volume = '28319'

                    var Eclipse_avg_buy = '9,945.20'
                    var Eclipse_avg_sell = '9,948.48'
                    var Eclipse_eff_pl = '-457,209'
                    var Eclipse_real_pl = '-225,456'
                    var Eclipse_yield = '-11.56%'
                    var Eclipse_volume = '20172'

                    var Floral_avg_buy = '470.32'
                    var Floral_avg_sell = '473.41'
                    var Floral_eff_pl = '3,047,975'
                    var Floral_real_pl = '1,487,244'
                    var Floral_yield = '26.11%'
                    var Floral_volume = '23798'

                    var Galactia_avg_buy = '7,402.58'
                    var Galactia_avg_sell = '7,697.18'
                    var Galactia_eff_pl = '7,069,586'
                    var Galactia_real_pl = '5,682,461'
                    var Galactia_yield = '-34.00%'
                    var Galactia_volume = '27763'

                    var Heliosphere_avg_buy = '8,584.65'
                    var Heliosphere_avg_sell = '8,746.38'
                    var Heliosphere_eff_pl = '-64,579,596'
                    var Heliosphere_real_pl = '-30,959,758'
                    var Heliosphere_yield = '14.13%'
                    var Heliosphere_volume = '16315'

                    var Interstella_avg_buy = '2,928.95'
                    var Interstella_avg_sell = '2,907.04'
                    var Interstella_eff_pl = '11,447,170'
                    var Interstella_real_pl = '6,080,025'
                    var Interstella_yield = '-28.89%'
                    var Interstella_volume = '20071'

                    var Jupiter_avg_buy = '4,137.01'
                    var Jupiter_avg_sell = '3,990.95'
                    var Jupiter_eff_pl = '11,398,613'
                    var Jupiter_real_pl = '4,473,554'
                    var Jupiter_yield = '28.49%'
                    var Jupiter_volume = '22271'

                    var Koronis_avg_buy = '2,501.18'
                    var Koronis_avg_sell = '2,467.95'
                    var Koronis_eff_pl = '34,293,417'
                    var Koronis_real_pl = '17,557,774'
                    var Koronis_yield = '-5.22%'
                    var Koronis_volume = '24559'

                    var Lunatic_avg_buy = '1,623.27'
                    var Lunatic_avg_sell = '1,577.53'
                    var Lunatic_eff_pl = '-2,491,616'
                    var Lunatic_real_pl = '-1,630,085'
                    var Lunatic_yield = '-31.86%'
                    var Lunatic_volume = '23337'


                    // APPENDS DATA VALUES INTO THE TABLE BELOW THE CHART
                    var table = document.getElementById("myTable");
                    table.rows[1].cells[1].innerHTML = total_avg_buy; // Total average buy price
                    table.rows[1].cells[2].innerHTML = total_avg_sell; // Total average sell price
                    table.rows[1].cells[3].innerHTML = total_eff_pl; // Total effective p/l
                    table.rows[1].cells[4].innerHTML = total_real_pl; // Total realised p/l
                    table.rows[1].cells[5].innerHTML = total_yield; // Total yield
                    table.rows[1].cells[6].innerHTML = total_volume; // Total volume
                    table.rows[1].style.backgroundColor = "#f5f5f5";
                    
                    table.rows[2].cells[1].innerHTML = Astronomica_avg_buy;
                    table.rows[2].cells[2].innerHTML = Astronomica_avg_sell;
                    table.rows[2].cells[3].innerHTML = Astronomica_eff_pl;
                    table.rows[2].cells[4].innerHTML = Astronomica_real_pl;
                    table.rows[2].cells[5].innerHTML = Astronomica_yield;
                    table.rows[2].cells[6].innerHTML = Astronomica_volume;
                  
                    table.rows[3].cells[1].innerHTML = Borealis_avg_buy;
                    table.rows[3].cells[2].innerHTML = Borealis_avg_sell;
                    table.rows[3].cells[3].innerHTML = Borealis_eff_pl;
                    table.rows[3].cells[4].innerHTML = Borealis_real_pl;
                    table.rows[3].cells[5].innerHTML = Borealis_yield;
                    table.rows[3].cells[6].innerHTML = Borealis_volume;

                    table.rows[4].cells[1].innerHTML = Celestial_avg_buy;
                    table.rows[4].cells[2].innerHTML = Celestial_avg_sell;
                    table.rows[4].cells[3].innerHTML = Celestial_eff_pl;
                    table.rows[4].cells[4].innerHTML = Celestial_real_pl;
                    table.rows[4].cells[5].innerHTML = Celestial_yield;
                    table.rows[4].cells[6].innerHTML = Celestial_volume;
      
                    table.rows[5].cells[1].innerHTML = Deuteronic_avg_buy;
                    table.rows[5].cells[2].innerHTML = Deuteronic_avg_sell;
                    table.rows[5].cells[3].innerHTML = Deuteronic_eff_pl;
                    table.rows[5].cells[4].innerHTML = Deuteronic_real_pl;
                    table.rows[5].cells[5].innerHTML = Deuteronic_yield;
                    table.rows[5].cells[6].innerHTML = Deuteronic_volume;
            
                    table.rows[6].cells[1].innerHTML = Eclipse_avg_buy;
                    table.rows[6].cells[2].innerHTML = Eclipse_avg_sell;
                    table.rows[6].cells[3].innerHTML = Eclipse_eff_pl;
                    table.rows[6].cells[4].innerHTML = Eclipse_real_pl;
                    table.rows[6].cells[5].innerHTML = Eclipse_yield;
                    table.rows[6].cells[6].innerHTML = Eclipse_volume;
            
                    table.rows[7].cells[1].innerHTML = Floral_avg_buy;
                    table.rows[7].cells[2].innerHTML = Floral_avg_sell;
                    table.rows[7].cells[3].innerHTML = Floral_eff_pl;
                    table.rows[7].cells[4].innerHTML = Floral_real_pl;
                    table.rows[7].cells[5].innerHTML = Floral_yield;
                    table.rows[7].cells[6].innerHTML = Floral_volume
           
                    table.rows[8].cells[1].innerHTML = Galactia_avg_buy;
                    table.rows[8].cells[2].innerHTML = Galactia_avg_sell;
                    table.rows[8].cells[3].innerHTML = Galactia_eff_pl;
                    table.rows[8].cells[4].innerHTML = Galactia_real_pl;
                    table.rows[8].cells[5].innerHTML = Galactia_yield;
                    table.rows[8].cells[6].innerHTML = Galactia_volume;
           
                    table.rows[9].cells[1].innerHTML = Heliosphere_avg_buy;
                    table.rows[9].cells[2].innerHTML = Heliosphere_avg_sell;
                    table.rows[9].cells[3].innerHTML = Heliosphere_eff_pl;
                    table.rows[9].cells[4].innerHTML = Heliosphere_real_pl;
                    table.rows[9].cells[5].innerHTML = Heliosphere_yield;
                    table.rows[9].cells[6].innerHTML = Heliosphere_volume
            
                    table.rows[10].cells[1].innerHTML = Interstella_avg_buy;
                    table.rows[10].cells[2].innerHTML = Interstella_avg_sell;
                    table.rows[10].cells[3].innerHTML = Interstella_eff_pl;
                    table.rows[10].cells[4].innerHTML = Interstella_real_pl;
                    table.rows[10].cells[5].innerHTML = Interstella_yield;
                    table.rows[10].cells[6].innerHTML = Interstella_volume;

                    table.rows[11].cells[1].innerHTML = Jupiter_avg_buy;
                    table.rows[11].cells[2].innerHTML = Jupiter_avg_sell;
                    table.rows[11].cells[3].innerHTML = Jupiter_eff_pl;
                    table.rows[11].cells[4].innerHTML = Jupiter_real_pl;
                    table.rows[11].cells[5].innerHTML = Jupiter_yield;
                    table.rows[11].cells[6].innerHTML = Jupiter_volume;
           
                    table.rows[12].cells[1].innerHTML = Koronis_avg_buy;
                    table.rows[12].cells[2].innerHTML = Koronis_avg_sell;
                    table.rows[12].cells[3].innerHTML = Koronis_eff_pl;
                    table.rows[12].cells[4].innerHTML = Koronis_real_pl;
                    table.rows[12].cells[5].innerHTML = Koronis_yield;
                    table.rows[12].cells[6].innerHTML = Koronis_volume;
            
                    
                    table.rows[13].cells[1].innerHTML = Lunatic_avg_buy;
                    table.rows[13].cells[2].innerHTML = Lunatic_avg_sell;
                    table.rows[13].cells[3].innerHTML = Lunatic_eff_pl;
                    table.rows[13].cells[4].innerHTML = Lunatic_real_pl;
                    table.rows[13].cells[5].innerHTML = Lunatic_yield;
                    table.rows[13].cells[6].innerHTML = Lunatic_volume
         

                });

            </script>
    <script src="https://d3js.org/d3.v4.min.js"></script>
    <script src="index.js"></script>
    <script type="text/javascript">


        var options = {
            chart: {
                type: 'spline',
                scrollablePlotArea: {
                    minWidth: 600,
                    scrollPositionX: 1
                }
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'datetime',
                labels: {
                    overflow: 'justify'
                },
                title: {
                    text: 'Time of Day'
                },
            },
            yAxis: {
                title: {
                    text: 'Dealing Price'
                },
                minorGridLineWidth: 0,
                gridLineWidth: 0,
                alternateGridColor: null,
                plotBands: [{ // Gentle breeze
                    from: 500,
                    to: 2500,
                    color: 'rgba(68, 170, 213, 0.1)',
                    label: {
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // Moderate breeze
                    from: 2500,
                    to: 5000,
                    color: 'rgba(0, 0, 0, 0)',
                    label: {
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // Fresh breeze
                    from: 5000,
                    to: 7500,
                    color: 'rgba(68, 170, 213, 0.1)',
                    label: {
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // Strong breeze
                    from: 7500,
                    to: 10000,
                    color: 'rgba(0, 0, 0, 0)',
                    label: {
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // High wind
                    from: 10000,
                    to: 13000,
                    color: 'rgba(68, 170, 213, 0.1)',
                    label: {
                        style: {
                            color: '#606060'
                        }
                    }
                }]
            },
            tooltip: {
                valueSuffix: ''
            },
            plotOptions: {
                spline: {
                    lineWidth: 2.5,
                    states: {
                        hover: {
                            lineWidth: 4
                        }
                    },
                    marker: {
                        enabled: false
                    },
                    pointInterval: 17000, // one hour
                    pointStart: Date.UTC(2018, 6, 30, 5, 55, 0)
                }
            },
            series: [],
            navigation: {
                buttonOptions: {
                    enabled: false
                },
                menuItemStyle: {
                    fontSize: '10px'
                }
            },
        }


        var processed_json = [];

        var received;


        Highcharts.ajax({
            url: "json/instruments.json",
            // type: "POST",
            success: function (data) {
                var numOfIns = data.instrumentList.length;

                for (i = 0; i < numOfIns; i++) {
                    var item = {},
                        deals = [];
                    numDeals = data.instrumentList[i].dealList.length;

                    item["name"] = data.instrumentList[i].instrumentName;
                    for (j = 0; j < numDeals; j++) {
                        deals.push(parseInt(data.instrumentList[i].dealList[j].dealAmount))
                    }

                    item["data"] = deals;
                    processed_json.push(item);
                }


                console.log("insideJSON", processed_json);
                options.series = processed_json;

                // console.log("hardData", am);
                console.log("jsonData", processed_json);
                console.log("series", options.series);


                Highcharts.chart('container', options);
            }
        });

    </script>
</head>
</br></br>
<body id="LoginForm" img
      src="https://www.google.com/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwiUwcvzo87cAhXIJFAKHSHuByMQjRx6BAgBEAU&url=http%3A%2F%2Fomninet.com%2F&psig=AOvVaw2-AnEz2Gf2WeEwla83eFFj&ust=1533296275503328">
<div class="container" style="width:80%">
    <div class="login-form" style="margin-top: 5%">
        <div class="main-div">
            <div class="panel">
                </br></br>
                <center><img
                        src="https://upload.wikimedia.org/wikipedia/commons/7/7b/Deutsche_Bank_logo_without_wordmark.svg"
                        width="70" height="70"></center>
                <h2 class="text-center"><br>Welcome to Deutsche Bank Investment Services</h2>
                <p class="text-center"><i>Please select the instrument(s) below that you would like to visualise for the
                    trading period: 30/07/2018 05:58-06:59
                </p></i>
            </div>
            <center>

                <button type="button" id="Toggle" class="btn btn-dark">
                    Switch View
                </button>
            </center>
            <div id="container" style="min-width: 310px; height: 600px; margin: 25px auto"></div>

            </br>

            <div id="table_buttons">

                <center>
                    <button type="button" id="ShowAll" class="btn btn-light">Select All</button>
                    <button type="button" id="All" class="btn btn-dark">Unselect All</button>
                </center>
                <p id=selected""></p>
            </div>
            <div id="table_options">


                <button type="button" style="margin: 2px 1px 2px 1px;" class="btn btn-info" id="Astronomica">
                    Astronomica
                </button>
                <button type="button" style="margin: 2px 1px 2px 1px;" class="btn btn-info" id="Borealis">Borealis
                </button>
                <button type="button" style="margin: 2px 1px 2px 1px;" class="btn btn-info" id="Celestial">Celestial
                </button>
                <button type="button" style="margin: 2px 1px 2px 1px;" class="btn btn-info" id="Deuteronic">Deuteronic
                </button>
                <button type="button" style="margin: 2px 1px 2px 1px;" class="btn btn-info" id="Eclipse">Eclipse
                </button>
                <button type="button" style="margin: 2px 1px 2px 1px;" class="btn btn-info" id="Floral">Floral</button>
                <button type="button" style="margin: 2px 1px 2px 1px;" class="btn btn-info" id="Galactia">Galactia
                </button>
                <button type="button" style="margin: 2px 1px 2px 1px;" class="btn btn-info" id="Heliosphere">
                    Heliosphere
                </button>
                <button type="button" style="margin: 2px 1px 2px 1px;" class="btn btn-info" id="Interstella">
                    Interstella
                </button>
                <button type="button" style="margin: 2px 1px 2px 1px;" class="btn btn-info" id="Jupiter">Jupiter
                </button>
                <button type="button" style="margin: 2px 1px 2px 1px;" class="btn btn-info" id="Koronis">Koronis
                </button>
                <button type="button" style="margin: 2px 1px 2px 1px;" class="btn btn-info" id="Lunatic">Lunatic
                </button
            </div>
            </br></br>
            <table class="table" id="myTable">

                <thead class="thead-dark">
                <tr class="Titles thead-dark">
                    <th>Instrument Name</th>
                    <th>Average Buy Price</th>
                    <th>Average Sell Price</th>
                    <th>Effective P&L</th>
                    <th>Realised P&L</th>
                    <th>Yield</th>
                    <th>Volume</th>
                </tr>
                </thead>

                <tbody>
                <tr class="Total">
                    <th><i>All Instruments</i></th>
                    <th><i>TBC</i></th>
                    <th><i>TBC</i></th>
                    <th><i>TBC</i></th>
                    <th><i>TBC</i></th>
                    <th><i>TBC</i></th>
                    <th><i>TBC</i></th>
                </tr>
                <tr class="Astronomica">
                    <td>Astronomica
                    </th>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                </tr>
                <tr class="Borealis">
                    <td>Borealis
                    </th>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                </tr>
                <tr class="Celestial">
                    <td>Celestial
                    </th>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                </tr>
                <tr class="Deuteronic">
                    <td>Deuteronic
                    </th>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                </tr>
                <tr class="Eclipse">
                    <td>Eclipse
                    </th>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                </tr>
                <tr class="Floral">
                    <td>Floral
                    </th>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                </tr>
                <tr class="Galactia">
                    <td>Galactia
                    </th>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                </tr>
                <tr class="Heliosphere">
                    <td>Heliosphere
                    </th>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                </tr>
                <tr class="Interstella">
                    <td>Interstellas
                    </th>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                </tr>
                <tr class="Jupiter">
                    <td>Jupiter
                    </th>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                </tr>
                <tr class="Koronis">
                    <td>Koronis
                    </th>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                </tr>
                <tr class="Lunatic">
                    <td>Lunatic
                    </th>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                    <td>TBC</td>
                </tr>
                <tbody>
            </table>

            <br>

        </div>

    </div>
</div>
</div>

</div>
</br></br>
</body>
</html>