<html>
<head>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="css/index.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->
</head>

<body id="LoginForm" img
      src="https://www.google.com/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwiUwcvzo87cAhXIJFAKHSHuByMQjRx6BAgBEAU&url=http%3A%2F%2Fomninet.com%2F&psig=AOvVaw2-AnEz2Gf2WeEwla83eFFj&ust=1533296275503328">
<div class="container">
    <div class="login-form text-center mx-auto" style="margin-top: 5%">
        <div class="main-div">
            <div class="panel">
                <img src="https://upload.wikimedia.org/wikipedia/commons/7/7b/Deutsche_Bank_logo_without_wordmark.svg"
                     width="70" height="70">

                <h2><br>Welcome</h2>
                <p><br>Deutsche Bank Trading Platform</p>
                <p>Please enter your email and password:</p>
            </div>

            <form id="Login" style="margin-top: 25px" method="POST" action="login">
                <div class="form-group">
                    <input type="text" class="form-control" id="inputEmail" name="email" value="${email}"
                           placeholder="Email Address">
                </div>

                <div class="form-group">
                    <input type="password" class="form-control" id="inputPassword" name="password"
                           placeholder="Password">
                </div>
                <p><br><span style="color:red">${errMessage}</span></p>

                <button type="submit" class="btn btn-info">Log in</button>
                <br><br>
                <button type="submit" class="btn btn-light" formaction="register" formmethod="GET">Not Registered?
                </button>
                <br>
            </form>

        </div>
    </div>
</div>


</body>
</html>
