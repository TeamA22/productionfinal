<html>
<head>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="css/index.css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type='text/javascript' src='js/jquery.ba-hashchange.min.js'></script>
    <script type='text/javascript' src='js/dynamicpage.js'></script>

    <script src="https://d3js.org/d3.v5.js"></script>
    <!------ Include the above in your HEAD tag ---------->
</head>

<body id="LoginForm" img
      src="https://www.google.com/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwiUwcvzo87cAhXIJFAKHSHuByMQjRx6BAgBEAU&url=http%3A%2F%2Fomninet.com%2F&psig=AOvVaw2-AnEz2Gf2WeEwla83eFFj&ust=1533296275503328">
<div class="container">
    <div class="login-form text-center mx-auto" style="margin-top: 5%">
        <div class="main-div">
            <div class="panel">
                <img src="https://upload.wikimedia.org/wikipedia/commons/7/7b/Deutsche_Bank_logo_without_wordmark.svg"
                     width="70" height="70">
                <h2><br>Welcome</br></h2>
                <p><br>Deutsche Bank Trading Platform</p>
                <p><i>Registration</p></i>
            </div>

            <form action="/register" method="POST" id="Register" style="margin-top: 25px">
                <div class="form-group">
                    <input type="email" class="form-control" name="email" placeholder="email"> <span style="color:red">${state}</span><br>
                </div>
                <div class="form-group">

                    <input type="password" class="form-control" name="password" placeholder="password"><br>
                </div>

                <div class="form-group">

                    <input type="password" class="form-control" name="password-conf"
                           placeholder="repeat password"><br><br>
                </div>
                <div class="form-group">

                    <input type="submit" class="btn btn-info" value="Submit">
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>