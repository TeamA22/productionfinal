package service;

import entity.UserProfile;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UserProfileServiceTest {

    private UserProfileService userProfileService;

    @Before
    public void setUp() {
        userProfileService = new UserProfileService();
    }

    @Test
    public void testContentService() {
        String login = "name";
        UserProfile userProfile = new UserProfile(login,
                "password",
                "email");
        userProfileService.addNewUserToLoginMap(userProfile);
        assertEquals("Should be inside",
                true,
                userProfileService.getUserByLogin(login) != null);
    }
}
